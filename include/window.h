
#include <gtkmm/window.h>
#include <gtkmm/label.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/listbox.h>
#include <gtkmm/image.h>
#include <gtkmm/filechooserdialog.h>
#include <glibmm.h>

class MainWindow : public Gtk::Window
{
    public :
        MainWindow(int width,int height, char title[]);
        ~MainWindow();
    private :
        Gtk::HeaderBar*   headerbar;
        Gtk::ListBox*     container;
        Gtk::Box*         input_box;
        Gtk::Image*       input_image;
        Gtk::Button*      input;
        Gtk::Label*       input_label;
        Gtk::Box*         output_box;
        Gtk::Label*       output_label;
        Gtk::Image*       output_image;
        Gtk::Button*      output;
        Gtk::Box*         model_box;
        Gtk::Label*       model_label;
        Gtk::Button*      model;
        Gtk::Button*      start_btn;
        
        Glib::ustring  model_path;
        Glib::ustring  input_file;
        Glib::ustring  output_path;
        int m_width, m_height;
        char *m_title;


    protected:
        void on_button_input_clicked();
        void on_button_output_clicked();
        void on_button_model_clicked();
        void on_input_dialog_response(int response_id, Gtk::FileChooserDialog* dialog);
        void on_output_dialog_response(int response_id, Gtk::FileChooserDialog* dialog);
        void on_model_dialog_response(int response_id, Gtk::FileChooserDialog* dialog);

        void upscale();
};

/* #ifndef GTKMM_EXAMPLEWINDOW_H
 * #define GTKMM_EXAMPLEWINDOW_H
 *
 * #include <gtkmm.h>
 *
 * class ExampleWindow : public Gtk::Window
 * {
 * public:
 *   ExampleWindow();
 *   virtual ~ExampleWindow();
 *
 * protected:
 *   //Signal handlers:
 *   void on_button_file_clicked();
 *   void on_button_folder_clicked();
 *   void on_file_dialog_response(int response_id, Gtk::FileChooserDialog* dialog);
 *   void on_folder_dialog_response(int response_id, Gtk::FileChooserDialog* dialog);
 *
 *   //Child widgets:
 *   Gtk::Box m_ButtonBox;
 *   Gtk::Button m_Button_File, m_Button_Folder;
 * };
 *
 * #endif //GTKMM_EXAMPLEWINDOW_H */
