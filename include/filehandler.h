#include <stdio.h>
#include <glibmm.h>

class FileHandler{
    public:
        FileHandler(char path[]);
        ~FileHandler();
        Glib::ustring get_text();
        void put_text(Glib::ustring in);
    private:
        Glib::ustring text;
        char* m_path;
        FILE* textfile;
        void m_read();
        void m_countsize();
        int m_count;

};
