cmake_minimum_required(VERSION 3.10)
set (CMAKE_CXX_STANDARD 20)
project(UIdesign)
find_package(PkgConfig REQUIRED)
pkg_check_modules(GTKMM REQUIRED gtkmm-4.0)
pkg_check_modules(GLIBMM REQUIRED glibmm-2.68)
message("Compiler Version: ${CMAKE_CXX_COMPILER_VERSION}")
set(sources 
	${CMAKE_SOURCE_DIR}/src/main.cpp
	${CMAKE_SOURCE_DIR}/src/window.cpp
	${CMAKE_SOURCE_DIR}/src/filehandler.cpp 
)
set(headers
	${CMAKE_SOURCE_DIR}/include/window.h
	${CMAKE_SOURCE_DIR}/include/filehandler.h
)
add_executable(${PROJECT_NAME} ${sources} ${headers})
link_directories(${GTKMM_LIBRARY_DIRS})
link_directories(${GLIBMM_LIBRARY_DIRS})
target_link_libraries(${PROJECT_NAME} ${GTKMM_LIBRARIES})
include_directories(${GTKMM_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} ${GLIBMM_LIBRARIES})
include_directories(${GLIBMM_INCLUDE_DIRS})
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_SOURCE_DIR}/include)
