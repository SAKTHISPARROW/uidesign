
#include <gtkmm/window.h>
#include <gtkmm/textview.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/viewport.h>
#include <gtkmm/textbuffer.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <filehandler.h>
#include <glibmm.h>

class MainWindow : public Gtk::Window
{
    public :
        MainWindow(int width,int height, char title[]);
        ~MainWindow();
    private :
        Gtk::TextView* editor;
        Gtk::ScrolledWindow* scroller;
        Glib::RefPtr<Gtk::TextBuffer> buffer;
        Gtk::Box*       container;
        Gtk::Box*       btn_box;
        Gtk::HeaderBar* headerbar;
        //Gtk::Viewport*  viewport;
        Gtk::Button*    save_btn;
        Gtk::Button*    quit_btn;

        

        FileHandler file;
        int m_width, m_height;
        char *m_title;
       
    protected:
        void p_save();
        void quit();
};

