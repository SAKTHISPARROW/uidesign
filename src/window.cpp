#include<window.h>
MainWindow::MainWindow(int width,int height,char title[])
: m_width(width)
, m_height(height)
{
    //setting up windows
    m_title = title;
    set_title(title);
    set_default_size(width,height);


    //defining elements
    headerbar     = new Gtk::HeaderBar();
    container     = new Gtk::ListBox();

    input_box     = new Gtk::Box(Gtk::Orientation::HORIZONTAL);
    input         = new Gtk::Button("Input");
    input_label   = new Gtk::Label("Input");
    input_image   = new Gtk::Image();

    output_label  = new Gtk::Label("Output");
    output_box    = new Gtk::Box(Gtk::Orientation::HORIZONTAL);
    output        = new Gtk::Button("Output");
    
    model_label   = new Gtk::Label("Model");
    model_box     = new Gtk::Box(Gtk::Orientation::HORIZONTAL);
    model         = new Gtk::Button("Model");
    start_btn     = new Gtk::Button("Start");

    container->set_margin(20);
    input_box->set_spacing(50);
    input->set_halign(Gtk::Align::START);
    input->set_valign(Gtk::Align::CENTER);


    output_box->set_spacing(50);
    model_box->set_spacing(50);
    output->set_halign(Gtk::Align::START);
    output->set_valign(Gtk::Align::CENTER);
    
    //building widgets
    set_child(*container);
    headerbar->set_show_title_buttons();
    set_titlebar(*headerbar);

    container->append(*input_box);
    container->append(*output_box);
    container->append(*model_box);
    
    container->append(*start_btn);
    container->set_child_visible();
    container->show();
    
    input_box->append(*input);
    output_box->append(*output);
    model_box->append(*model);

    //Signals and slots
    //save_btn->signal_clicked().connect(sigc::mem_fun(*this,&MainWindow::p_save));
    input->signal_clicked().connect(sigc::mem_fun(*this,
                &MainWindow::on_button_input_clicked) );
    
     output->signal_clicked().connect(sigc::mem_fun(*this,
                  &MainWindow::on_button_output_clicked) );

    model->signal_clicked().connect(sigc::mem_fun(*this,
                &MainWindow::on_button_model_clicked) );
    
    start_btn->signal_clicked().connect(sigc::mem_fun(*this,
                &MainWindow::upscale) );
}
MainWindow::~MainWindow()
{
    delete container;
    delete input;
    delete input_label;
    delete input_image;
    delete input_box;
    delete output_image;
    delete output_label;
    delete output_box;
    delete output;
    delete model;
    delete headerbar;
    delete start_btn;
}



void MainWindow::on_button_input_clicked()
{
  auto dialog = new Gtk::FileChooserDialog("Please choose a file",
          Gtk::FileChooser::Action::OPEN);
  dialog->set_transient_for(*this);
  dialog->set_modal(true);
  dialog->signal_response().connect(sigc::bind(
    sigc::mem_fun(*this, &MainWindow::on_input_dialog_response), dialog));

  //Add response buttons to the dialog:
  dialog->add_button("_Cancel", Gtk::ResponseType::CANCEL);
  dialog->add_button("_Open", Gtk::ResponseType::OK);


  auto filter_any = Gtk::FileFilter::create();
  filter_any->set_name("Any files");
  filter_any->add_pattern("*");
  dialog->add_filter(filter_any);

  //Show the dialog and wait for a user response:
  dialog->show();
}

void MainWindow::on_input_dialog_response(int response_id, Gtk::FileChooserDialog* dialog)
{
  //Handle the response:
  switch (response_id)
  {
    case Gtk::ResponseType::OK:
    {
      //Notice that this is a std::string, not a Glib::ustring.
      input_file = Glib::ustring(dialog->get_file()->get_path());
      input_label->set_text(input_file);
      input_box->append(*input_label);

      Glib::RefPtr<Gdk::Pixbuf> imagefile = Gdk::Pixbuf::create_from_file(input_file);
      input_image->set_size_request(200,200);

      input_image->set(imagefile);
      input_box->append(*input_image);
      break;
    }
    case Gtk::ResponseType::CANCEL:
    {
      printf("Cancel clicked.");
      break;
    }
    default:
    {
      printf("Unexpected button clicked.");
      break;
    }
  }
  delete dialog;
}


void MainWindow::on_button_model_clicked()
{
  auto dialog = new Gtk::FileChooserDialog("Please choose a folder",
          Gtk::FileChooser::Action::SELECT_FOLDER);
  dialog->set_transient_for(*this);
  dialog->set_modal(true);
  dialog->signal_response().connect(sigc::bind(
    sigc::mem_fun(*this, &MainWindow::on_model_dialog_response), dialog));

  //Add response buttons to the dialog:
  dialog->add_button("_Cancel", Gtk::ResponseType::CANCEL);
  dialog->add_button("Select", Gtk::ResponseType::OK);

  //Show the dialog and wait for a user response:
  dialog->show();
}

void MainWindow::on_model_dialog_response(int response_id, Gtk::FileChooserDialog* dialog)
{
  //Handle the response:
  switch (response_id)
  {
    case Gtk::ResponseType::OK:
    {
      auto folder = dialog->get_file()->get_path();
      model_path = folder;
      model_label->set_text(folder);
      model_box->append(*model_label);
      break;
    }
    case Gtk::ResponseType::CANCEL:
    {
      printf("Cancel clicked.");
      break;
    }
    default:
    {
      printf("Unexpected button clicked.");
      break;
    }
  }
  delete dialog;
}



void MainWindow::on_button_output_clicked()
{
  auto dialog = new Gtk::FileChooserDialog("Please choose a folder",
          Gtk::FileChooser::Action::SELECT_FOLDER);
  dialog->set_transient_for(*this);
  dialog->set_modal(true);
  dialog->signal_response().connect(sigc::bind(
    sigc::mem_fun(*this, &MainWindow::on_output_dialog_response), dialog));

  //Add response buttons to the dialog:
  dialog->add_button("_Cancel", Gtk::ResponseType::CANCEL);
  dialog->add_button("Select", Gtk::ResponseType::OK);

  //Show the dialog and wait for a user response:
  dialog->show();
}

void MainWindow::on_output_dialog_response(int response_id, Gtk::FileChooserDialog* dialog)
{
  //Handle the response:
  switch (response_id)
  {
    case Gtk::ResponseType::OK:
    {
      output_path = dialog->get_file()->get_path();
      output_label->set_text(output_path);
      output_box->append(*output_label);
      break;
    }
    case Gtk::ResponseType::CANCEL:
    {
      printf("Cancel clicked.");
      break;
    }
    default:
    {
      printf("Unexpected button clicked.");
      break;
    }
  }
  delete dialog;
}

void
MainWindow::upscale(){
    std::string command = "cd "+ model_path +" && python "+model_path+"/inference_codeformer.py -w 0.1 " +" -i \""+input_file+"\" -o \""+ output_path + "\" --bg_upsampler realesrgan"+"--face_upsample";
    system(command.c_str());
    command = "open "+output_path;
    system(command.c_str());
    std::string basename = input_file.substr(input_file.find_last_of("/")+1);
    output_image = new Gtk::Image(output_path + "/final_results/"+basename);
    container->append(*output_image);
}
