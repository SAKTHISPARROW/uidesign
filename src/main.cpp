#include <window.h>
#include <gtkmm/application.h>
#include<memory>

static void
on_activate (Glib::RefPtr<Gtk::Application> app)
{
	MainWindow *window = new MainWindow(1280,720,"Title");
	app->add_window(*window);
	window->present();
}

int
main (int argc, char *argv[])
{
	int ret;

	Glib::RefPtr<Gtk::Application> app =
		Gtk::Application::create("github.example.com");
	app->signal_activate().connect(sigc::bind(&on_activate, app));
	ret = app->run(argc, argv);

	return ret;
}
