#include <filehandler.h>

FileHandler::FileHandler(char path[]){
    printf("%s",path);
    m_path = path;
    textfile = fopen(path,"r");
    if(!textfile){
        text = "Unable to open file!";
    }
    else{
        //m_countsize();
        m_read();
    }
}

FileHandler::~FileHandler(){
  fclose(textfile);
}

void
FileHandler::put_text(Glib::ustring data){
    if(!!textfile){
        fclose(textfile);
    }
    else{
      printf("Text file not open!\n");
    }
    textfile = fopen(m_path,"w");
    const char* tdata = data.c_str();
    fprintf(textfile,"%s",tdata);
}


void
FileHandler::m_countsize(){
    char ch;
    while((ch = getc(textfile)) != EOF){
        m_count++;
    }

    fseek(textfile,0,SEEK_SET);
}
void
FileHandler::m_read(){
    char ch;
    while((ch = getc(textfile)) != EOF){
        text.push_back(ch);
    }
}

Glib::ustring
FileHandler::get_text(){
    return text;
}
